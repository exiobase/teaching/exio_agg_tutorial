""" Small example on how to load one pymrio matrix into pandas
"""

import pandas as pd
from pathlib import Path

Z_file = Path('./data/exio3_agg/Z.txt')
F_stressor_file = Path('./data/exio3_agg/satellite/F.txt')
FY_stressor_file = Path('./data/exio3_agg/satellite/F_Y.txt') 

# The square matrices have two col/index = [0,1]
Z = pd.read_csv(Z_file, sep='\t', header=[0,1], index_col=[0,1])

# The stressors here have one index col [0]
F = pd.read_csv(F_stressor_file, sep='\t', header=[0,1], index_col=[0])
FY = pd.read_csv(FY_stressor_file, sep='\t', header=[0,1], index_col=[0])

# To have a look first you can explore with. This loads the first 10 rows and you can explore how many index rows/cols there are, then set the header / index_col accordingly
F_test = pd.read_csv(F_stressor_file, sep='\t', nrows=10)


