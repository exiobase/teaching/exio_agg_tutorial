# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Tutorial: aggregating EXIOBASE with Pymrio

# A small tutorial for aggregating EXIOBASE 3 into a XXX region xxx sector system with pymrio.

from pathlib import Path

import pymrio
import openpyxl
import pandas as pd
import polyfuzz 

# # Loading the concordance data

# First we want to check what actually is in the workbook.
# Obviously, we could do that in open office/excel but we can also stay completely in Python:

AGG_DEF_FILE = Path('./EXIO3_7r23p.xlsx')

# We use [openpyxl](https://openpyxl.readthedocs.io/en/stable/) here which allow direct interactions with xlsx (not xls! - use [xlrd](https://xlrd.readthedocs.io/en/latest/) for these) files.

workbook = openpyxl.load_workbook(filename=AGG_DEF_FILE)

# Lets see which sheets we have in the workbook:

workbook.sheetnames

# Looks like with some aggregation data for 'sectors', 'countries' and the final demand 'Y'

# Lets load these into python with [pandas](https://pandas.pydata.org/)

# As a first try with just load all the data without parameters

sec_conc = pd.read_excel(io=AGG_DEF_FILE, sheet_name='sectors')

# Lets have a look:

sec_conc.shape

# So we have 200 rows, which corresponds to the EXIOBASE 3 product size - so this is good.
# And we have 24 columns - so on the first glance it looks like we aggregate to 24 sectors.

# Lets have a glance on the structure:

sec_conc.head()

# So the first columns actually contains the names of the EXIOBASE product names (so it turns out we will aggregate to 23 sectors).
# We can put them into the index for the concordance by either:

# 1. Move one of the columns to the index

exio_index_col = sec_conc.columns[0]
sec_conc.set_index(exio_index_col, drop=True, inplace=True)
sec_conc.index.name = 'exio3'
sec_conc.columns.name = 'new'

# **OR**
#
# 2. From the very beginning load the data with the first column set to the index

sec_conc = pd.read_excel(io=AGG_DEF_FILE, sheet_name='sectors', index_col=0)
sec_conc.index.name = 'exio3'
sec_conc.columns.name = 'new'

# Now it is just a matter of repeating the code for the sheets.
# We *could* just copy the code above and adjust it for the different sheets - but [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).
# So instead we are doing it in a loop and store the concordance data in a dict:

conc = {conc: pd.read_excel(io=AGG_DEF_FILE, sheet_name=conc, index_col=0) for conc in workbook.sheetnames}

# Lets have a look 

conc['Y'].head()
conc['countries'].head()

# So we have a lot of NaN in the concordances. So we need to remove them - again, we *could* do that afterwards, but we can also write a small loading function with does all the cleaning and renaming.

def load_sheet(filename, sheet):
    """ Load sheet from filenname including replacing nan and casting to integers """
    df = pd.read_excel(io=filename, sheet_name=sheet, index_col=0).fillna(0).astype(int)
    df.index.name = 'exio3'
    df.columns.name = 'new'
    df.index=df.index.str.replace("'", "")  # some countries are stored with ''
    return df

# And with that we can load the data again

conc = {conc: load_sheet(AGG_DEF_FILE, conc) for conc in workbook.sheetnames}

# And now it looks like we are good to go:

conc['countries'].head()

# # Getting EXIOBASE 

# We will use EXIOBASE version 3 for the year 2019 in the product by product (pxp) classification.
# Pymrio can [directly download that](https://pymrio.readthedocs.io/en/latest/notebooks/autodownload.html) from the scientific data repository [Zenodo](https://doi.org/10.5281/zenodo.3583070).

# Note that we will download following https://doi.org/10.5281/zenodo.3583070 - this DOI will always resolve to the latest available EXIOBASE 3 version.
# If you visit this repo, you will find a section "versions" with a list of dois.  If you want to specify a previous EXIOBASE 3 version (you can pass this to download_exiobase3 as parameter doi=)

# To prepare for the download we first need to specify a folder where we can download the data and the parameters of the MRIO

EXIO_DATA_FOLDER = Path('./data/exio/')
YEAR = 2019
SYSTEM = 'pxp'

# Then we can start the download - this might take some time (it downloads about 700 MB). **Note** that the file will only be downloaded if it does not exist in the given folder.

exio_meta = pymrio.download_exiobase3(
    storage_folder=EXIO_DATA_FOLDER, system=SYSTEM, years=YEAR
)

# After the download we can check the meta data record which provides some provenance info on the data sources and history

exio_meta

# The download routine downloads the compressed (zip) file. We can keep the file compressed, as Pymrio can read the data directly.
# We can look for the file with

dir_content = list(EXIO_DATA_FOLDER.glob('*'))
dir_content

# So this contains the metadata.json (the file storing the meta data we have checked before) and the zip file containing the mrio data.
# We can load the data from this file with (again, depending on your PC this might take some time)

exio3 = pymrio.parse_exiobase3(path=EXIO_DATA_FOLDER / 'IOT_2019_pxp.zip')

# Again, with exio3.meta we have a log track of the things which happened to the MRIO. Currently, this is not linked to the download meta but this will be implemented in a future version.

exio3.meta

# We can investigate the classifications in EXIOBASE with

exio3.get_sectors()

exio3.get_regions()

exio3.get_Y_categories()

# # Checking and adjusting concordance specifications

# It is a good idea to check if the classification of EXIOBASE 3 matches the one of the aggregation concordance:

# Lets check the regions first

assert (exio3.get_regions() == conc['countries'].index).all(), "Regions do not match"

# So this looks fine, next the final demand categories

assert (exio3.get_Y_categories() == conc['Y'].index).all(), " Final demand categories do not match"

# Looks also fine

# And then the sectors

assert (exio3.get_sectors() == conc['sectors'].index).all(), "Index does not match"

# So here we get an AssertionError - these indexes do not match, lets have a closer look:

# First we have the sectors in exio3 which are not the concordance file

exio3_sec_none_match = list(exio3.get_sectors().difference(conc['sectors'].index))
exio3_sec_none_match

# and here the opposite for concordance data

conc_sec_none_match = list(conc['sectors'].index.difference(exio3.get_sectors()))
conc_sec_none_match

# Looking at these two lists we find that the names are different and that we can not just match them by position either.
# However, it also looks like we could match this list based on the wording without any further information.
# We will use the package [PolyFuzz](https://github.com/MaartenGr/PolyFuzz/) for a first match


pfm = polyfuzz.PolyFuzz("TF-IDF")
pfm.match(conc_sec_none_match, exio3_sec_none_match)

# Lets check the matches

matches = pfm.get_matches()
matches

# Looks mostly ok, but we see that some entries of the concordance matrix are linked to the same entry

matches[matches.duplicated(subset='To', keep=False)]

# So as a first step we can order the matches by similarity score, and then only keep the first occurrence of each match

matches_first_round = matches.sort_values(by='Similarity', ascending=False).drop_duplicates(subset='To', keep='first')
matches_first_round 

# Next, we remove the already matched entries from the two unmatched list we had before

exio3_sec_still_no_match = list(set(exio3_sec_none_match).difference(set(matches_first_round.To)))
conc_sec_still_no_match = list(set(conc_sec_none_match).difference(set(matches_first_round.From)))

# And repeat the step above

pfm.match(conc_sec_still_no_match, exio3_sec_still_no_match)
matches_second_round = pfm.get_matches()

# Lets combine the two matches

all_matches = pd.concat([matches_first_round, matches_second_round]) 
all_matches

# *Tip:* We can also inspect the matches in a spreadsheet (paste after the command) by calling 

all_matches.to_clipboard() 

# Now prepare a dictionary we can use for renaming the index in the correspondence dataframe.

sec_rename_dict = all_matches.loc[:, ['From', 'To']].set_index( 'From' ).to_dict()['To']


# And rename the values in the correspondence dataframe.
# Here we make use of the name we assigend to the index before ('exio3'): 
# We first make the index a column (reset_index), then replace the names (replace) and then put the index back.
# We can get away for not specifiying a column for the replace as we are sure that the only strings are in the 'exio3' column

conc['sectors'] = conc['sectors'].reset_index().replace(to_replace=sec_rename_dict).set_index('exio3')

# And now check the sectors again

assert (exio3.get_sectors() == conc['sectors'].index).all(), "Index does not match"

# **And this passes!**

# # Aggregating EXIOBASE 3

# Now we can start aggregating the exio3 system:

exio3_agg = exio3.copy()

# *Notes:* Currently we need to transform the pandas DataFrame into a numpy array and extract the new names for 
# the aggregated sectors and regions. In a future version of pymrio it will be possible to directly pass
# a concordance given as a pandas DataFrame.

exio3_agg.aggregate(region_agg=conc['countries'].to_numpy().T, region_names=list(conc['countries'].columns),
             sector_agg=conc['sectors'].to_numpy().T, sector_names=list(conc['sectors'].columns) )

# Currently, pymrio lacks functionality to directly aggregate the final demand (Y). 
# However, it is quite easy to do that manually:

exio3_agg.Y = pd.concat({reg: exio3_agg.Y.loc[:, reg] @ conc['Y'] for reg in exio3_agg.get_regions()}, axis=1)

# And there we have it, a fully aggregated MRIO system. 
# Lets explore a bit:

exio3_agg.meta

exio3_agg.get_regions()

exio3_agg.get_sectors()

exio3_agg.get_Y_categories()

# We can now use it to calculate footprints and other accounts::

exio3_agg.calc_all()

exio3_agg.impacts.D_cba_reg

exio3_agg.save_all('./data/exio3_agg')

# And due to current events, let look into country footprints after cancelling Russian trade:

# First we reset the system to redo the calculation

exio3_agg_RU_isolated = exio3_agg.copy()
exio3_agg_RU_isolated.reset_all_full()
Z = exio3_agg_RU_isolated.Z
Y = exio3_agg_RU_isolated.Y


# Reducing the Russian domestic activity to 70%, trade to 20%

for reg in exio3_agg_RU_isolated.get_regions():
    if reg == 'Russia':
        Z.loc[(reg, slice(None)), (reg, slice(None))] = Z.loc[(reg, slice(None)), (reg, slice(None))] * 0.7
        Y.loc[(reg, slice(None)), (reg, slice(None))] = Y.loc[(reg, slice(None)), (reg, slice(None))] * 0.7
    else:
        Z.loc[('Russia', slice(None)), (reg, slice(None))] = Z.loc[('Russia', slice(None)), (reg, slice(None))] * 0.2
        Y.loc[('Russia', slice(None)), (reg, slice(None))] = Y.loc[('Russia', slice(None)), (reg, slice(None))] * 0.2
        Z.loc[(reg, slice(None)), ('Russia', slice(None))] = Z.loc[(reg, slice(None)), ('Russia', slice(None))] * 0.2
        Y.loc[(reg, slice(None)), ('Russia', slice(None))] = Y.loc[(reg, slice(None)), ('Russia', slice(None))] * 0.2


exio3_agg_RU_isolated.Z = Z
exio3_agg_RU_isolated.Y = Y

exio3_agg_RU_isolated.calc_all()

# Here are the footprints with Russia isolated and reduced domestic economic activity

exio3_agg_RU_isolated.impacts.D_cba_reg

# And lets compare it with the original data:

exio3_agg.impacts.D_cba_reg



