# EXIOBASE aggregation tutorial

A short example notebook on how to use [Pymrio](https://github.com/konstantinstadler/pymrio) for aggregating [EXIOBASE](https://doi.org/10.5281/zenodo.3583070).

Content:

- A notebook (./exio_agg_tutorial.ipynb) and script (./exio_agg_tutorial.py - same content as the notebook) showing the steps of getting an aggregated EXIOBASE system
- the source data (EXIOBASE 3 product by product for 2019): in /data/exio
- the aggregated data in /data/exio3_agg
- the definitions for the aggregation: ./EXIO3_7r23p.xlsx
- an environment.yml file defining all packages for a conda environment for running the code:

    ~~~
        conda env create -f environment.yml
        conda activate exio_agg
    ~~~
